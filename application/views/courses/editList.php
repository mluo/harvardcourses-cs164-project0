<?
/***********************************************************************
 * editList.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Allows users to edit shopping or taking list by selecting which
 * courses they want to keep. Resets list in localStorage.
 **********************************************************************/?>
<form>
    <div data-role="fieldcontain">    
        <fieldset data-role="controlgroup">
			<label for="flipCourses">Keep:</label>
			<select name="flipCourses" id="flipCourses" data-role="slider">
				<option value="off" selected="selected">None</option>
				<option value="on">All</option>
			</select><br><br>
 
            <fieldset data-role="listview" id="list">
            </fieldset>
        </fieldset>
    </div>
</form>

<a data-role="button" onclick="submit('<?= $listType ?>')">Submit</a>

<!-- hidden link so that js can redirect to view the list just modified -->
<span style="display:none" id="link"><?= base_url('lists/' . $listType) ?></span>

<script>
    /*
     * displayBoxes(x)
     *
     * displays checkboxes for list x (shopping or taking)
     */
    function displayBoxes(x) {  
        if ((localStorage.getItem(x + "CatNum")) == "")
            return;
      
        // retreive cat numbers of courses in list & split into an array
	    var catNums = localStorage.getItem(x + "CatNum");
	    catNums = catNums.split("$ ");
	    
        // retreive course titles of courses in list & split into an array
	    var courseTitles = localStorage.getItem(x + "CourseTitle");
	    courseTitles = courseTitles.split("$ ");
	    
	    var n = catNums.length;
        var stuff = "";
        
	    for (var i = 0; i < n; i++) {
			stuff += '<input type="checkbox" name="' + catNums[i] + '"value="' + courseTitles[i] + '" id="' + catNums[i] + '" class="courses" >';
				stuff += '<label for="' + catNums[i] + '">' + courseTitles[i] + '</label>';
	    }
	    
	    $("#list").html(stuff);
    }

    /*
     * flip(id, category)
     *
     * Looks at slider "id" to see if it's on or off. Checks or unchecks all the checkboxes in "category".
     */    
    function flip(id, category) {
        if (id.val() == "on")
            $("." + category).attr("checked", true).checkboxradio("refresh");
        if (id.val() == "off")
            $("." + category).attr("checked", false).checkboxradio("refresh");
    }

    // bind flip function to courses slider
    $("#flipCourses").change(function () {
        flip($(this), "courses");
    });

    /*
     * submit(x)
     *
     * Iterates through checkboxes on the page and adds checked courses back to list x (shopping or taking)
     */     
    function submit(x) {
        // create string of catalog numbers, separated by $ delimiter
        var catNums = $('input:checked').map(function() {
            return $(this).attr('name');
        }).get().join("$ ");
        
        // create string of course titles, separated by $ delimiter
        var courseTitles = $('input:checked').map(function() {
            return $(this).attr('value');
        }).get().join("$ ");
        
      // reset list x
      localStorage.setItem(x + "CatNum", catNums);
      localStorage.setItem(x + "CourseTitle", courseTitles);
      
      // redirect to view list that was just modified
      window.location = $("#link").html();
    }
</script>


<? if ($shopping): ?>
    <script>displayBoxes("shopping")</script>

<? elseif ($taking): ?>
    <script>displayBoxes("taking")</script>
<? endif ?>


