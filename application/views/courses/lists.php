<!--********************************************************************
 * list.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Displays results of a search, popular courses, or lists (recent, 
 * shopping, taking).
 *********************************************************************-->

<!-- container to hold course links -->
<div id="list"></div>

<script>
    /*
     * show(x)
     *
     * displays list x (either recent, shopping, or taking)
     */
    function show(x) {
        if ((localStorage.getItem(x + "CatNum")) == "")
            return;
    
        // retreive cat numbers of courses in list & split into an array
	    var catNums = localStorage.getItem(x + "CatNum");
	    catNums = catNums.split("$ ");
	    
        // retreive course titles of courses in list & split into an array
	    var courseTitles = localStorage.getItem(x + "CourseTitle");
	    courseTitles = courseTitles.split("$ ");
	    
	    var n = catNums.length;
        var stuff = "";
		
		// write html for links to courses in the list
	    for (var i = 0; i < n; i++) {
	        stuff += '<li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">';
	            stuff += '<div class="ui-btn-inner ui-li">';
                    stuff +='<div class="ui-btn-text">';
	                    stuff += '<a href="<?= base_url("/course") ?>' + "/" + catNums[i] + '" class="ui-link-inherit" data-ajax="false" data-transition="slide">';
			                stuff += courseTitles[i];
	                            stuff += '</a>';
	                        stuff += '</div>';
	                    stuff += '<span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>';
	                stuff += '</div>';
	            stuff += '</li>';
	    }
		
	    $("#list").html(stuff);
	}
</script>


<? if ($recent) : ?>
    <a data-role="button" onclick="clearRecents()">Clear Recents</a>
    <script>show("recents")</script>
    <script>
        /*
         * clearRecents()
         *
         * Clears out recents list in localStorage
         */
        function clearRecents() {
            // set catalog numbers and course titles to empty strings
            localStorage.removeItem("recentsCatNum");
		    localStorage.removeItem("recentsCourseTitle");
		    		    
		    //uncomment to clear everything out
		    /*
		    localStorage.removeItem("shoppingCatNum");
		    localStorage.removeItem("shoppingCourseTitle");
		    localStorage.removeItem("takingCatNum");
		    localStorage.removeItem("takingCourseTitle");*/
		    
		    // refresh page to reflect changes
		    location.reload();
        }
    </script>

<? elseif ($shopping) : ?>
    <a href="<?= base_url('/editList/shopping') ?>" data-role="button" data-ajax="false">Edit Shopping List</a>
    <script>show("shopping")</script>

<? elseif ($taking) : ?>
    <a href="<?= base_url('/editList/taking') ?>" data-role="button" data-ajax="false">Edit Taking List</a>
    <script>show("taking")</script>

<? else : ?>
    <!-- if not a recents, shopping, or taking list, use php to write out code,
         using data passed in by controller -->
    <? foreach ($coursesList as $course) : ?>
	    <li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
	        <div class="ui-btn-inner ui-li">
                <div class="ui-btn-text">
	                <a href="<?= base_url('course/' . $course['catNum']) ?>" class="ui-link-inherit" data-ajax="false" data-transition="slide">
			            <?= htmlspecialchars($course["deptCode"]) . " " . htmlspecialchars($course["num"]) . " - " . htmlspecialchars($course["title"]) ?>
	                </a>
	            </div>
	            <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
	        </div>
	    </li>
    <? endforeach ?>
<? endif ?>
