<?
/***********************************************************************
 * browse.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Displays list of categories by which to browse courses and 
 * expandable/collapsable lists of categories to filter by.
 **********************************************************************/?>

<form method="post" accept-charset="utf-8" action="<?= base_url('browse') ?>" data-ajax="false" data-transition="slide"/>
    <button type="submit" data-theme="c" name="submit" value="submit-value" class="ui-btn-hidden" aria-disabled="false" data-ajax="false">Go!</button>
	
	<div data-role="collapsible-set">
	
		<!-- departments -->
		<div data-role="collapsible" data-content-theme="c">
			<h3>Departments</h3>
			<fieldset data-role="controlgroup">

				<div data-role="fieldcontain">
					<label for="flipDepts">Select:</label>
					<select name="flipDepts" id="flipDepts" data-role="slider">
						<option value="off">None</option>
						<option value="on" selected="selected">All</option>
					</select> 
				</div>
				
				<? foreach ($deptCodesAndTitles as $deptCT) : ?>
					<input class="depts" type="checkbox" name="depts[<?= $deptCT['deptCode'] ?>]" id="<?= $deptCT['deptCode'] ?>" checked="checked" />
						<label for="<?= $deptCT['deptCode'] ?>"><?= htmlspecialchars($deptCT['dept']) ?></label>
				<? endforeach ?>
				
			</fieldset>
		</div>
		
		<!-- geneds -->
		<div data-role="collapsible" data-content-theme="c">
			<h3>Gen Eds</h3>
			<p><fieldset data-role="controlgroup">
			
				<div data-role="fieldcontain">
					<label for="flipGenEds">Select:</label>
					<select name="slider" id="flipGenEds" data-role="slider">
						<option value="off" selected="selected">None</option>
						<option value="on">All</option>
					</select> 
				</div>
				
				<? foreach ($genEdCodesAndTitles as $code => $title) : ?>
					<input type="checkbox" name="genEds[<?= $code ?>]" id="<?= $code ?>" class="genEds"/>
						<label for="<?= $code ?>"><?= htmlspecialchars($title) ?></label>
				<? endforeach ?>
			</fieldset></p>
		</div>
		
		<!-- days -->
		<div data-role="collapsible" data-content-theme="c">
			<h3>Days</h3>
			<p><fieldset data-role="controlgroup">
			
				<div data-role="fieldcontain">
					<label for="flipDays">Select:</label>
					<select name="slider" id="flipDays" data-role="slider">
						<option value="off">None</option>
						<option value="on" selected="selected">All</option>
					</select> 
				</div>
				
				<? foreach ($dayCodesAndTitles as $code => $day) : ?>
						<input type="checkbox" name="days[<?= $code ?>]" id="<?= $code ?>" class="days" checked="checked" />
							<label for="<?= $code ?>"><?= htmlspecialchars($day) ?></label>
				<? endforeach ?>
			</fieldset></p>
		</div>
		
		<!-- times -->
		<div data-role="collapsible" data-content-theme="c">
			<h3>Times</h3>
			<p><div data-role="fieldcontain"><fieldset data-role="controlgroup">
			    <legend></legend>
			    
			    <label for="begin">Start</label>
                <select name="begin" id="begin">
                    <option value="900">Start</option>
                    <? for ($i = 9; $i <= 22; $i++) : ?>
                        <option value="<?= $i * 100 ?>"><?= $i ?>:00</option>                            
                        <option value="<?= $i * 100 + 30 ?>"><?= $i ?>:30</option>
                    <? endfor ?>
                </select>
                
                <label for="end">End</label>
                <select name="end" id="end">
                    <option value="2200">End</option>
                    <? for ($i = 9; $i <= 22; $i++) : ?>
                        <option value="<?= $i * 100 ?>"><?= $i ?>:00</option>                            
                        <option value="<?= $i * 100 + 30 ?>"><?= $i ?>:30</option>
                    <? endfor ?>
                </select>
			</fieldset></div></p>
		</div>
	</div>
	
</form>

<script>
    /*
     * flip(id, category)
     *
     * Looks at slider "id" to see if it's on or off. Checks or unchecks all the
     * checkboxes in "category".
     */
    function flip(id, category) {
        if (id.val() == "on")
            $("." + category).attr("checked", true).checkboxradio("refresh");
        if (id.val() == "off")
            $("." + category).attr("checked", false).checkboxradio("refresh");
    }

    // bind flip function to sliders
    $("#flipDepts").change(function () {
        flip($(this), "depts");
    });
    
    $("#flipGenEds").change(function () {
        flip($(this), "genEds");
    });
    
    $("#flipDays").change(function () {
        flip($(this), "days");
    });
</script>


