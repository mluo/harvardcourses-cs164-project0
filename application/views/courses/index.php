<?
/***********************************************************************
 * index.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Home page of app. Displays search bar. Uses JS to submit.
 **********************************************************************/ ?>
 
<form method="post" name="query" action="<?= base_url('search')?>" accept-charset="utf-8"/>
	<label for="search-basic" class="ui-hidden-accessible">Search:</label>
	<input type="search" name="query" id="search-basic" value="" />
</form>

