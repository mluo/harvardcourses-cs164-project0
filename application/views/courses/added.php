<?
/***********************************************************************
 * added.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Displays confimation method when course is added to shopping or 
 * taking list.
 **********************************************************************/?>
<!doctype html>
    <! -- doesn't use template header because it only needs to display a confirmation message -->
    <head> 
		<title><?= $title ?></title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.css" />
		<link rel="stylesheet" type="text/css" href="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css" /> 
		<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script>
	</head> 

    <body>
	    <div data-role="page">
		    <div data-role="header">
			    <h1><?= $title ?></h1>
		    </div><!-- /header -->
        
            <div data-role="content">You have successfully added <?= $deptCode . " " . $num ?> to your <?= $listType ?> list!</div>
        </div>
    </body>

</html>
