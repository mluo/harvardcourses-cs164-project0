<!--********************************************************************
 * course.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Displays information for a single course.
 *********************************************************************-->
<!-- course info -->
<p><?= htmlspecialchars($courseData["desc"]) ?></p>

<strong>Department : </strong>
	<?= htmlspecialchars($courseData["dept"]) ?><br>
<strong>Course Code : </strong>
	<span id="deptCode"><?= htmlspecialchars($courseData["deptCode"]) ?></span>
	<span id="num"><?= htmlspecialchars($courseData["num"]) ?></span><br>
<strong>Instructor(s) : </strong>
	<?= htmlspecialchars($courseData["facultyString"]) ?><br>
<strong>Satisfies : </strong>
	<?= htmlspecialchars($courseData["genEdsString"]) ?><br>
<strong>Location : </strong>
	<?= htmlspecialchars($courseData["location"]) ?><br>
<strong>Catalog Number : </strong>
	<span id="catNum"><?= $courseData["catNum"] ?></span><br>
<strong>Meets : </strong>
	<?= htmlspecialchars($courseData["meets"]) ?><br>

<!-- hidden element to hold course title to pass to js function -->
<span style="display:none" id="courseTitle">
    <?= $courseData["deptCode"] . " " . $courseData["num"] . " - " . $courseData["title"] ?>
</span>

<a data-rel="dialog" data-role="button" data-inline="true" onclick="addTo('shopping')"> Add to Shopping</a>
<a data-rel="dialog" data-role="button" data-inline="true" onclick="addTo('taking')">Add to Taking</a>

<!-- links to confirmation page when course successfully added to shopping or taking list; hidden elements triggered when add function successfullly completes -->
<a style="display:none" id="shoppingConfirm" href="<?= base_url('added/shopping') . '/' .  $courseData['deptCode'] . '/' . $courseData['num'] ?>" data-rel="dialog"></a>
<a style="display:none" id="takingConfirm" href="<?= base_url('added/taking') . '/' .  $courseData['deptCode'] . '/' . $courseData['num'] ?>" data-rel="dialog"></a>

<script>
    // add course to recents when page finishes loading
    $(document).ready(function() {
      addTo("recents");
    });
    
    /*
     * addTo(x)
     *
     * adds this course to list x (recents, shopping, or taking)
     */
    function addTo(x) {
        try {
            // create new localStorage item if it doesn't exist already
            if (!(localStorage.getItem(x + "CatNum"))) {
                var catNums = $("#catNum").html();
                var courseTitles = $("#courseTitle").html();
            }
            
            // otherwise just append onto the old one
            else {
           	    var catNums = $("#catNum").html() + "$ " + localStorage.getItem(x + "CatNum");
           	    var courseTitles = $("#courseTitle").html() + "$ " + localStorage.getItem(x + "CourseTitle");
		    }
		    
		    // reset the localStorage item		    
		    localStorage.setItem(x + "CatNum", catNums);
		    localStorage.setItem(x + "CourseTitle", courseTitles);
		    
		    // confirm that course has been successfully added to shopping or taking list by triggering link to the confirmation page
		    if (x != "recents")
    		    $("#" + x + "Confirm").trigger("click");
		    
	    } catch (e) {
		    if (e == QUOTA_EXCEEDED_ERR) {
			    alert("Quota exceeded!");
		    }
		 }
	}
</script>
	
