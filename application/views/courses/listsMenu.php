<?
/***********************************************************************
 * listsMenu.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Stylized links to pages with shopping and taking lists.
 **********************************************************************/?>

	<li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
	    <div class="ui-btn-inner ui-li">
            <div class="ui-btn-text">
	            <a href="<?= base_url('lists/shopping') ?>" class="ui-link-inherit" data-ajax="false">
			        Courses I'm Shopping
	            </a>
	        </div>
	        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
	    </div>
	</li>
	
	<li data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
	    <div class="ui-btn-inner ui-li">
            <div class="ui-btn-text">
	            <a href="<?= base_url('lists/taking') ?>" class="ui-link-inherit" data-ajax="false">
			        Courses I'm Taking
	            </a>
	        </div>
	        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
	    </div>
	</li>
