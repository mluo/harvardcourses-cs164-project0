<!--********************************************************************
 * templates/header.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Default header to be used on every page.
 *********************************************************************-->
<? $this->load->helper('url') ?>
<? //$this->load->helper('asset') ?>

<!doctype html>
	<head> 
		<title><?= htmlspecialchars($title) ?></title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.css" />
		<link rel="stylesheet" type="text/css" href="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css" />
		<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.0/jquery.mobile-1.0.min.js"></script> 		
		<link rel="stylesheet" type="text/css" href="http://project0/css/Project0.css" /> 
		<link rel="stylesheet" type="text/css" href="http://project0/css/Project0.min.css" /> 
	</head> 

	
	<body>
	<div data-role="page">
		<div data-role="header">
		    <a data-rel="back" data-icon="back" data-iconpos="notext" data-transition="slide">back</a>
			<h1><?= htmlspecialchars($title) ?></h1>
			<a href="<?= base_url() ?>" data-role="button" data-icon="home" data-iconpos="notext">home</a>
		</div><!-- /header -->
