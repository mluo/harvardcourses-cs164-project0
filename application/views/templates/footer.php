<!--********************************************************************
 * templates/footer.php
 *
 * Michelle Luo and Evan Wu
 * Computer Science 164
 * Project 0
 *
 * Default footer to be used on every page. Displays 5 button options.
 *********************************************************************-->
		
		    <br>
		    <div data-role="footer" data-id="myfooter" data-position="fixed">
		        <div data-role="navbar">
		            <ul>
			            <li><a href="<?= base_url('browseMenu') ?>" data-icon="gear" data-ajax="false">
				            Browse
			            </a></li>
			            <li><a href="<?= base_url('lists/popular') ?>" data-icon="star" data-ajax="false">
				            Popular
			            </a></li>
			            <li><a href="<?= base_url('lists/recent') ?>" data-icon="back" data-ajax="false">
				            Recent
			            </a></li>
			            <li><a href="<?= base_url('index') ?>" data-icon="search" data-ajax="false">
				            Search
			            </a></li>
			            <li><a href="<?= base_url('listsMenu') ?>" data-icon="grid" data-ajax="false">
				            Lists
			            </a></li>
			        </ul>
		        </div>
		    </div>
	    </div><!-- </page> -->
	</body>
</html>
