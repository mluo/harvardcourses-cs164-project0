<?
/*****************************************************************************
 * courses_model.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 0
 *
 * Model file. Called by courses.php controller.
 * Includes methods for retrieving controller-requested data from SQL 
 * databases. Methods generally prepare SQL query statements based on 
 * controller input and then return the query results back to the controller.
 ****************************************************************************/

class Courses_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function getAllCourses() {
        // don't need * from table because only need to generate list of courses
        $query = $this->db->query("SELECT catNum, deptCode, 
            num, title FROM courses ORDER BY deptCode;");
        return $query->result_array();
    
    }
    
    public function getPopularCourses() {
        $query = $this->db->query("SELECT catNum, deptCode,
        	    num, title FROM courses ORDER BY views DESC LIMIT 10;");
    	return $query->result_array();
    	    // each element of array can be accessed with 
    	    // foreach ($array as $row) $row["title"]; etc.
    }
    
    public function getDepts() {
        // this method simply returns a list of all of the stored departments and their codes
        $query = $this->db->query("SELECT * FROM departments;");
        return $query->result_array();
    }
    
    public function getCourse($catNum, $whatInfo = "basics") {
        // this method provides the data necessary to generate a course's individual page
        
        // update number of views for course - for popularity ranking purposes
        $this->db->query("UPDATE courses SET views = views + 1 WHERE catNum =" . $catNum . ";");
        
        // retrieve either courses table ("basics") data or faculty ("faculty") table data
        if ($whatInfo == "basics") {
            $query = $this->db->query("SELECT * FROM courses JOIN departments 
                ON courses.deptCode = departments.deptCode WHERE courses.catNum = $catNum;"); 
            return $query->row_array();
        }
        if ($whatInfo == "faculty") {
            $query = $this->db->query("SELECT facName FROM faculty WHERE catNum = $catNum;");
            return $query->result_array();
        }
    }
    
    public function searchCourses($searchString) {
        // this method searches titles, descriptions, catalog numbers, and faculty names for a keyword
        
        // we need the DISTINCT because otherwise will return duplicates of classes with >1 faculty (because of JOIN)        
        $sqlQuery = "SELECT DISTINCT courses.catNum, courses.deptCode, courses.num, courses.title 
            FROM courses
            JOIN faculty ON faculty.catNum = courses.catNum
            WHERE";
            
        // want all permutations of search terms
        $keywords = explode(" ", $searchString);
        foreach ($keywords as $keyword) {
            $sqlQuery = $sqlQuery . 
                " (courses.title LIKE '%$keyword%' OR 
                courses.desc LIKE '%$keyword%' OR 
                courses.catNum LIKE '%$keyword%' OR
                faculty.facName LIKE '%$keyword%' OR
                courses.num LIKE '%$keyword%' OR
                courses.deptCode LIKE '%$keyword%') AND";
        } 
        
        // trim excess AND
        $sqlQuery = substr($sqlQuery, 0, -3);
        
        // get courses from database
        $query = $this->db->query($sqlQuery);  
        return $query->result_array();
    }
    

    public function browseCourses() {
        // this method finds courses that match the criteria the user wishes to browse by
       
        /* this method assumes that, in $_POST, there will be an array called depts[] with deptCode => boolean key-value pairs
         * there will also be similar arrays called genEds and days, as well as variables start and end
         * DONE: this requires that, in view forms, checkboxes are like <input name="depts[<? echo $deptCT['deptCode'] ?\>]">
         * 
         * $parameters = {$depts, $genEds, $days, $start, $end} - first three are arrays
         * $genEds = {$USW => T, $SPU => F, $SW, $SLS, $CB, $AI, $EMR, $ER, $SP} - these are key-value booleans 
         * $days = {$m => T, $t => F, $w, $th, $f} - these are key-value booleans */
         
        // get $_POST data 
        $parameters = $this->input->post();
         
        // make the sql query in pieces 
        $sql = "SELECT catNum, deptCode, num, title FROM courses WHERE ";

        // for each browsing criteria, put together options as OR boolean statement
        // department criteria
        foreach ($parameters["depts"] as $deptCode => $deptBoolean) {
            if ($deptBoolean = "on")
                $substringsDept[] = "deptCode = \"" . $deptCode . "\"";
        }
        // we only want to make implode this section if user has made a selection
        // if no selection, means that the user does not care about this criteria
        if (isset($substringsDept))
            $strings["depts"] = "(" . implode(" OR ", $substringsDept) . ")";
        
        // Gen Eds criteria
        foreach ($parameters["genEds"] as $genEdCode => $genEdBoolean) {
            if ($genEdBoolean = "on")
                $substringsGenEd[] = $genEdCode . " = TRUE";
        }
        if (isset($substringsGenEd))
            $strings["genEds"] = "(" . implode(" OR ", $substringsGenEd) . ")";
        
        // days criteria
        foreach ($parameters["days"] as $day => $dayBoolean) {
            if ($dayBoolean = "on")
                $substringsDays[] = $day . " = TRUE";  
        }  
        if (isset($substringsDays))
            $strings["days"] = "(" . implode(" OR ", $substringsDays) . ")";
        
        // we only have users choose a general begin and end time, not one for every day
        // TODO: should these be OR or AND?
        $strings["times"] = "(mBegin >= " . $parameters["begin"]  .
                           " OR tBegin >= " . $parameters["begin"] .
			               " OR wBegin >= " . $parameters["begin"] .
			               " OR thBegin >= " . $parameters["begin"] .
			               " OR fBegin >= " . $parameters["begin"] . ") AND " .
			               "(mEnd <= " . $parameters["end"]  .
                           " OR tEnd <= " . $parameters["end"] .
			               " OR wEnd <= " . $parameters["end"] .
			               " OR thEnd <= " . $parameters["end"] .
			               " OR fEnd <= " . $parameters["end"] . ")";

        // select only courses that work with all browsing criteria (depts, genEds, days, times)
        // join browsing component strings with AND boolean
        $sql = $sql . implode(" AND ", $strings) . "ORDER BY deptCode;" ;

        $query = $this->db->query($sql);
        return $query->result_array();
    }
}


?>
