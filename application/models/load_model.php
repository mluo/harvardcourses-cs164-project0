<?
/*****************************************************************************
 * load_model.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 0
 *
 * Model file. Called by load.php controller.
 * Parses XML file of courses and loads the SQL database with relevant data.
 ****************************************************************************/

class Load_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function loadDatabase() {
        $this->db->query("DELETE FROM courses");
        $this->db->query("DELETE FROM faculty");
        $this->db->query("DELETE FROM departments");
        
        $courseCatalog = new SimpleXMLElement($this->config->item("coursesXML"), NULL, TRUE);
        
        // go through each course in the XML file
        foreach ($courseCatalog->course as $course) {
        
            // only load courses offered spring 2012
            if ($course->attributes()->offered != "Y" 
                && $course->term->attributes()->spring_term != "Y") {
                continue;
            }
                
            // database has three tables: courses, departments, faculty
            
            // let's first just prepare for the courses table           
            $catNum = (int) $course->attributes()->cat_num;
            $deptCodeRaw = (string) $course->course_group->attributes()->code;
            // let's get rid of non alpha-numeric characters in the deptCode since CodeIgniter disallows them for GET/POST requests
            $deptCode = preg_replace("/[^a-zA-Z0-9\s]/", "", $deptCodeRaw);
            $num = (string) $course->course_number->num_int . $course->course_number->num_char;
            $title = (string) $course->title;
            $desc = (string) $course->description;
            
            // get location data, unless not provided
            $location = "Unknown";
            if ($course->meeting_locations->count() > 0) {
                foreach ($course->meeting_locations->location as $locationChild) {
                    $locationAttributes = $locationChild->attributes();
                    if ($locationAttributes["term"] == "2")
                        $location = (string) $locationAttributes["building"] 
                                    . ": " . (string) $locationAttributes["room"];
                    }
            }
            
            // get day and time info
            foreach ($course->schedule->meeting as $meeting) {
                $meetingAttributes = $meeting->attributes();
                if ($meetingAttributes["day"] == "1") {
                    $this->db->set("m", TRUE);
                    $this->db->set("mBegin", (int) $meetingAttributes["begin_time"]);
                    $this->db->set("mEnd", (int) $meetingAttributes["end_time"]);  
                }
                else if ($meetingAttributes["day"] == "2") {
                    $this->db->set("t", TRUE);
                    $this->db->set("tBegin", (int) $meetingAttributes["begin_time"]);
                    $this->db->set("tEnd", (int) $meetingAttributes["end_time"]);  
                }   
                else if ($meetingAttributes["day"] == "3") {
                    $this->db->set("w", TRUE);
                    $this->db->set("wBegin", (int) $meetingAttributes["begin_time"]);
                    $this->db->set("wEnd", (int) $meetingAttributes["end_time"]);   
                }   
                else if ($meetingAttributes["day"] == "4") {
                    $this->db->set("th", TRUE);
                    $this->db->set("thBegin", (int) $meetingAttributes["begin_time"]);
                    $this->db->set("thEnd", (int) $meetingAttributes["end_time"]);   
                }   
                else if ($meetingAttributes["day"] == "5") {
                    $this->db->set("f", TRUE);
                    $this->db->set("fBegin", (int) $meetingAttributes["begin_time"]);
                    $this->db->set("fEnd", (int) $meetingAttributes["end_time"]);  
                }       
            }
            
            // get Gen Ed info
            foreach($course->requirements->requirement as $requirement) {
                $requirementAttributes = $requirement->attributes();
                if ($requirementAttributes["name"] == "United States in the World")
                    $this->db->set("USW", TRUE);
                else if ($requirementAttributes["name"] == "Aesthetic and Interpretive Understanding")
                    $this->db->set("AI", TRUE);
                else if ($requirementAttributes["name"] == "Societies of the World")
                    $this->db->set("SW", TRUE);
                else if ($requirementAttributes["name"] == "Culture and Belief")
                    $this->db->set("CB", TRUE);
                else if ($requirementAttributes["name"] == "Science of the Physical Universe")
                    $this->db->set("SPU", TRUE);
                else if ($requirementAttributes["name"] == "Science of Living Systems")
                    $this->db->set("SLS", TRUE);
                else if ($requirementAttributes["name"] == "Study of the Past")
                    $this->db->set("SP", TRUE);
                else if ($requirementAttributes["name"] == "Empirical and Mathematical Reasoning")
                    $this->db->set("EMR", TRUE);
                else if ($requirementAttributes["name"] == "Ethical Reasoning")
                    $this->db->set("ER", TRUE);
                    
            }
            
            // prepare insertion of row into courses table
            $this->db->set("catNum", $catNum);
            $this->db->set("deptCode", $deptCode);
            $this->db->set("num", $num);
            $this->db->set("title", $title);
            $this->db->set("location", $location);
            $this->db->set("desc", $desc);
            // insert into courses table
            $this->db->insert("courses");
            
            // now let's insert into the departments table
            $dept = (string) $course->course_group;
            $this->db->query("INSERT IGNORE INTO departments (dept, deptCode) 
                VALUES ('{$dept}', '{$deptCode}');");
            
            // finally let's insert into the faculty table
            foreach ($course->faculty_list->faculty as $faculty) {
                if ($faculty->attributes()->term == "2") {
                   $facName = (string) $faculty->name->first . " " .
                        (string) $faculty->name->last;
                   
                   $this->db->set("facName", $facName);
                   $this->db->set("catNum", $catNum);
                   $this->db->insert("faculty");   
                }
            }   
        // end foreach course
        }    
    // end method loadDatabase        
    }
// end class Load_model
}

?>
    
    
