<?
/*****************************************************************************
 * courses.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 0
 *
 * Controller file. Calls courses_model.php model.
 * Includes methods for handling website after database has been loaded.
 * Methods generally get data from model, ready it for consumption by the
 * view, and then pass it to the view via the renderPage private method.
 ****************************************************************************/


class Courses extends CI_Controller {
    
    private static $genEdCodesAndTitles = 
        array("USW" => "United States in the World", 
                "SPU" => "Science of the Physical Universe",
                "SLS" => "Science of Living Systems",
                "AI" => "Aesthetic and Interpretive Understanding",
                "EMR" => "Empirical and Mathematical Reasoning",
                "ER" => "Ethical Reasoning",
                "SW" => "Societies of the World",
                "CB" => "Culture and Belief",
                "SP" => "Study of the Past");
    private static $dayCodesAndTitles = 
        array("m" => "Monday",
              "t" => "Tuesday",
              "w" => "Wednesday",
              "th" => "Thursday",
              "f" => "Friday"); 


    public function __construct() {
        parent::__construct();
        $this->load->model("courses_model");
    }

    public function index() {
        // index page is just a search bar
        $meta["methodName"] = "index";
        $meta["title"] = "HarvardCourses Mobile";
        
        // renderPage() takes meta and data arguments: 
        // meta is for header and footer views, data is for the main view
        $this->renderPage($meta, NULL);
    }
    
    public function added($list = NULL, $deptCode = NULL, $num = NULL) {
        // this method displays page to confirm adding a course to a shopping or taking list
        if ($list == NULL || $deptCode == NULL || $num == NULL)
            return;
        
        $data["title"] = ucfirst($list);
        
        $data["listType"] = $list;
        $data["deptCode"] = $deptCode;
        $data["num"] = $num;
        
        // doesn't user renderPage because it doesn't need a header or footer
        $this->load->view("courses/added", $data);
    }
    
    public function search($query = NULL) {
        // this method processes searches and displays results in list
        $meta["methodName"] = "lists";
        $meta["title"] = "Search Results";

        $data["recent"] = false;
        $data["shopping"] = false;
        $data["taking"] = false;

        // get $_POST data 
        $parameters = $this->input->post();
        
        // if user enters nothing in search bar, return all courses
        if ($parameters["query"] == "") 
            $data["coursesList"] = $this->courses_model->getAllCourses();
        // otherwise, perform search
        else 
            $data["coursesList"] = $this->courses_model->searchCourses($parameters["query"]);
        
        $this->renderPage($meta, $data); 
    }
    
    public function browseMenu() {
        // this method displays the menu for choosing options by which you can browse courses
        $meta["methodName"] = "browse";
        $meta["title"] = "Browsing Options";
        $data["deptCodesAndTitles"] = $this->courses_model->getDepts();
        
        $data["genEdCodesAndTitles"] = self::$genEdCodesAndTitles;
        $data["dayCodesAndTitles"] = self::$dayCodesAndTitles;
        $this->renderPage($meta, $data);
    }
    
    public function browse() {
        // this method directs browse processing and generates list of resulting courses
        $meta["methodName"] = "lists";
        $meta["title"] = "Browsing Results";

        $data["recent"] = false;
        $data["shopping"] = false;
        $data["taking"] = false; 
        
        $data["coursesList"] = $this->courses_model->browseCourses();
        $this->renderPage($meta, $data);
    }
    
    
    public function listsMenu() {
        // this method displays the menu of lists: recent, popular, taking, shopping
        $meta["methodName"] = "listsMenu";
        $meta["title"] = "Your Lists";
        
        $this->renderPage($meta, NULL);
    }
        
    
    public function lists($listType = "none") {
        // this method processes what list the user has chosen and redirects to correct one
        
        if ($listType == "none") {
            $this->index(); 
            return; 
        }    
        
        $meta["methodName"] = "lists";
        
        // this data is needed so the list view knows what to display
        $data["recent"] = false;
        $data["shopping"] = false;
        $data["taking"] = false; 
            
        // only need SQL data from model if getting popular courses    
        if ($listType == "popular") {
            $meta["title"] = "Popular";
        	$data["coursesList"] = $this->courses_model->getPopularCourses();
        }
        // if not, then prepare to render one of the other lists
        else {
            $meta["title"] = ucfirst($listType);
        	$data[$listType] = true;
        }

        $this->renderPage($meta, $data);
    }
    

    public function course($catNum = "none") {
        // this method generates the page displaying information for a specific courses
        
        if ($catNum == "none") {
            $this->index();
            return;
        }
        
        $meta["methodName"] = "course";
         
        // get basic data from model
        $data["courseData"] = $this->courses_model->getCourse($catNum, "basics");
        $meta["title"] = $data["courseData"]["deptCode"] . " " . $data["courseData"]["num"] . " - " . $data["courseData"]["title"]; 
        
        // get faculty data and parse faculty names into string
        $facultyData = array();
        $facultyRawData = $this->courses_model->getCourse($catNum, "faculty");
        foreach ($facultyRawData as $faculty)
            $facultyData[] = $faculty["facName"];
        $data["courseData"]["facultyString"] = implode(", ", $facultyData);
        
        // parse genEds into string
        $genEdsData = array();
        foreach (self::$genEdCodesAndTitles as $code => $title) {
            if ($data["courseData"]["$code"] == 1)
                $genEdsData[] = $title;   
        }
        $data["courseData"]["genEdsString"] = implode(", ", $genEdsData);
            
        // create a string that summarizes meeting times
        $meetings = array();
        foreach (self::$dayCodesAndTitles as $day => $dayName) {
            // check if course meets on certain day
            if ($data["courseData"][$day]) {
                // determine whether start time is on hour or half-hour
                if ($data["courseData"][$day . "Begin"]%100 == 0)
                    $startHalfHour = "00";
                else {
                    $startHalfHour = "30";
                    $data["courseData"][$day . "Begin"] -= 30;
                }
                
                // determine whether end time is on hour or half hour
                if ($data["courseData"][$day . "End"]%100 == 0)
                    $endHalfHour = "00";
                else {
                    $endHalfHour = "30";
                    $data["courseData"][$day . "End"] -= 30;
                }
            
                // add formatted time for given day to array
                $meetings[] = ucfirst($day) . " " . ($data["courseData"][$day . "Begin"]/100) . ":" .
                              $startHalfHour . "-" .
                              ($data["courseData"][$day . "End"]/100) . ":" .
                              $endHalfHour;  
            }      
        }
        
        // convert array of meeting times to single string
        $data["courseData"]["meets"] = implode(", ", $meetings);
        
        $this->renderPage($meta, $data);    
    }
    
    public function editList($listType = NULL) {
        // this method displays a form where users can edit a shopping or taking list
        if ($listType == NULL)
            return;
        
        $meta["methodName"] = "editList";
        $meta["title"] = "Edit " . ucfirst($listType) . " List";
        
        $data["shopping"] = false;
        $data["taking"] = false; 
        
        // let view know which type of list is being used
        $data[$listType] = true;
        
        $data["listType"] = $listType;
        
        // no other data needs to be passed to view because shopping/taking lists are in localStorage
        
        $this->renderPage($meta, $data);
    }
    
    private function renderPage($meta, $data) {
        // this method is responsible for ultimately rendering views
        $this->load->view("templates/header", $meta);
        $this->load->view("courses/" . $meta["methodName"], $data);
        $this->load->view("templates/footer", $meta);  
    }  
}
