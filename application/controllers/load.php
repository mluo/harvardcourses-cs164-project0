<?
/*****************************************************************************
 * load.php
 * 
 *
 * Evan Wu and Michelle Luo
 * Computer Science 164
 * Project 0
 *
 * Controller file. Calls load_model.php model.
 * Simply directs index.php/courses/load to load_model.php, which loads
 * the database with course catalog data.
 ****************************************************************************/

class Load extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('load_model');
    }
    
    public function index() {
        $this->load_model->loadDatabase();
    }
}

?>







